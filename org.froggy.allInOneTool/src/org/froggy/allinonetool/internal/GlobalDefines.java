package org.froggy.allinonetool.internal;

import java.text.DateFormat;

public class GlobalDefines {
	
	public static final DateFormat DATEFORMAT = DateFormat.getDateInstance();
	
	public static String FOLDERNAME = "";

	public static final String CONFIG = System.getProperty("user.dir") + "\\config\\config";
}
