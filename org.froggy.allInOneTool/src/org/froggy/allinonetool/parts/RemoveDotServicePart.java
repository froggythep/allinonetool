package org.froggy.allinonetool.parts;

import java.io.File;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public class RemoveDotServicePart extends CheckboxTreeViewerServicePart {

	private Button btnRemoveDots;
	
	private RemoveDotsAction removeDotsAction;

	@Override
	public void addAdditionalElements(Composite parent) {
		btnRemoveDots = new Button(parent, SWT.PUSH);
		btnRemoveDots.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		btnRemoveDots.setText("Remove Dots");
	}

	@Override
	protected void addListener() {
		super.addListener();
		btnRemoveDots.addSelectionListener(new RemoveDotsSelectionListener());
	}

	@Override
	protected void makeActions() {
		super.makeActions();
		removeDotsAction = new RemoveDotsAction();
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------LISTENER----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------

	private class RemoveDotsSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			removeDotsAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("default");
		}
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------ACTION------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------

	private class RemoveDotsAction {

		private void removeDots(File file) {
			if (file.isFile()) {
				String[] splitted = file.getPath().split(Pattern.quote("."));
				String newName = "";

				int i = 0;
				while (i < splitted.length - 1) {
					newName += splitted[i];
					if (i < splitted.length - 2) {
						newName += " ";
					}
					i++;
				}
				newName.trim();
				newName += "." + splitted[i];
				newName.trim();
				file.renameTo(new File(newName));

			} else {
				// is folder. Nothing to do here
			}
		}

		private void run() {
			final Object[] files = getViewer().getCheckedElements();
			
			createProgressShell();
			progressShell.open();
			progressBar.setMaximum(files.length);
			
			int jobNr = 0;
			for (Object file : files) {
				if (file instanceof File) {
					removeDots((File) file);
				}
				jobNr++;
				progressBar.setSelection(jobNr);
			}
			initData();
			progressShell.close();
		}
	}
}
