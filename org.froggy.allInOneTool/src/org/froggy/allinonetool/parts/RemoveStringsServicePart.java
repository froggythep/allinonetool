package org.froggy.allinonetool.parts;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.froggy.allinonetool.dialogs.StringOverviewDialog;
import org.froggy.allinonetool.internal.GlobalDefines;

public class RemoveStringsServicePart extends CheckboxTreeViewerServicePart {

	private Text txtInput;

	private Button btnAdd;

	private Button btnRun;

	private Button btnShowStrings;

	// ACTIONS
	private AddStringAction addStringAction;

	private ShowStringsAction showStringsAction;

	private RunRemovalAction runRemovalAction;

	@Override
	public void addAdditionalElements(Composite parent) {
		Label lbl = new Label(parent, SWT.NONE);
		lbl.setText("Here you can choose a part of the String that should be removed");
		lbl.setLayoutData(new GridData(SWT.None, SWT.None, false, false, 2, 1));
		txtInput = new Text(parent, SWT.BORDER);
		txtInput.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		btnAdd = new Button(parent, SWT.PUSH);
		btnAdd.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		btnAdd.setText("Add");
		btnAdd.addSelectionListener(new AddStringSelectionListener());

		btnShowStrings = new Button(parent, SWT.PUSH);
		btnShowStrings.setText("Show Strings");
		btnShowStrings.addSelectionListener(new ShowStringsSelectionListener());
		btnShowStrings.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		btnRun = new Button(parent, SWT.PUSH);
		btnRun.setText("Remove given Strings from File names");
		btnRun.addSelectionListener(new RunRemovalSelectionListener());
		btnRun.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));

	}

	@Override
	protected void addListener() {
		super.addListener();
		getViewer().addSelectionChangedListener(new TreeViewerSelectionChangedListener());
	}

	@Override
	protected void makeActions() {
		super.makeActions();
		addStringAction = new AddStringAction();
		showStringsAction = new ShowStringsAction();
		runRemovalAction = new RunRemovalAction();
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------LISTENER----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------

	private class TreeViewerSelectionChangedListener implements ISelectionChangedListener {

		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			try {
				if (((TreeSelection) event.getSelection()) != null) {
					File selectedFile = (File) ((TreeSelection) event.getSelection()).getPaths()[0].getLastSegment();
					txtInput.setText(selectedFile.getName());
				}
			} catch (ArrayIndexOutOfBoundsException e) {
			}

		}
	}

	private class AddStringSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			addStringAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("default");
		}
	}

	private class ShowStringsSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			showStringsAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("default");
		}
	}

	private class RunRemovalSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			runRemovalAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("default");
		}

	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------ACTION------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------

	private class AddStringAction {
		public void run() {
			try {
				boolean existing = false;
				String toAdd = txtInput.getText();
				List<String> lines = Files.readAllLines(Paths.get(GlobalDefines.CONFIG), StandardCharsets.UTF_8);
				for (String string : lines) {
					if (string.equals(toAdd)) {
						existing = true;
						break;
					}
				}

				if (!existing) {
					String currentConfig = new String(Files.readAllBytes(Paths.get(GlobalDefines.CONFIG)),
							StandardCharsets.UTF_8);
					currentConfig += toAdd + System.getProperty("line.separator");

					BufferedWriter writer = new BufferedWriter(new FileWriter(GlobalDefines.CONFIG));
					writer.write(currentConfig);
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class ShowStringsAction {
		public void run() {
			Display.getCurrent().getActiveShell();
			Dialog d = new StringOverviewDialog(Display.getCurrent().getActiveShell());
			d.open();
		}
	}

	private class RunRemovalAction {
		private List<String> config = new ArrayList<String>(0);

		public void run() {
			try {
				config = Files.readAllLines(Paths.get(GlobalDefines.CONFIG), StandardCharsets.UTF_8);
			} catch (IOException e) {
				System.out.println("Cannot read Config File");
			}

			Object[] files = getViewer().getCheckedElements();
			
			createProgressShell();
			progressShell.open();
			progressBar.setMaximum(files.length);

			int jobNr = 0;
			for (Object file : files) {
				if (file instanceof File) {
					renameFiles((File) file);
				}
				jobNr++;
				progressBar.setSelection(jobNr);
			}
			progressShell.close();
			initData();
		}
		private void renameFiles(File file) {
			if (file.isFile()) {
				for (String confLine : config) {
					if (file.getName().contains(confLine)) {
						file.renameTo(new File(file.getPath().replace(confLine, "")));
					}
				}
			} else {
				// is folder. Nothing to do here
			}
		}
	}

}