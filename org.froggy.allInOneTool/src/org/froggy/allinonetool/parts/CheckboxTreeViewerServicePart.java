package org.froggy.allinonetool.parts;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.froggy.allinonetool.helper.GuiHelper;
import org.froggy.allinonetool.internal.GlobalDefines;

public abstract class CheckboxTreeViewerServicePart {
	
	private CheckboxTreeViewer viewer;

	private Button btnSelectAll;
	
	private Button btnDeselectAll;

	private SelectAllAction selectAllAction;
	
	private DeselectAllAction deselectAllAction;
	
	protected Shell progressShell;
	
	protected ProgressBar progressBar;

	// STATICS
	private static final int MAX_GRID_SIZE = 2;
	
	@Inject
	public MDirtyable dirty;
	
	public abstract void addAdditionalElements(Composite parent);

	@PostConstruct
	public void createComposite(Composite parent) {
		parent.setLayout(new GridLayout(MAX_GRID_SIZE, false));
		parent.setBackground(new Color(Display.getCurrent(), 255, 255, 255));
		
		viewer = GuiHelper.generateFolderTreeViewer(parent, MAX_GRID_SIZE);
		
		btnSelectAll = new Button(parent, SWT.PUSH);
		btnSelectAll.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		btnSelectAll.setText("Select All");
		
		btnDeselectAll = new Button(parent, SWT.PUSH);
		btnDeselectAll.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		btnDeselectAll.setText("Deselect All");

		addAdditionalElements(parent);
		
		addListener();
		makeActions();
		initData();
	}
	
	protected CheckboxTreeViewer getViewer() {
		return viewer;
	}
	
	protected void addListener() {
		btnSelectAll.addSelectionListener(new SelectAllSelectionListener());
		btnDeselectAll.addSelectionListener(new DeselectAllSelectionListener());
	}

	protected void makeActions() {
		selectAllAction = new SelectAllAction();
		deselectAllAction = new DeselectAllAction();
	}

	protected void initData() {
		InitDataRunnable runnable = new InitDataRunnable();
		BusyIndicator.showWhile(Display.getCurrent(), runnable);
		runnable.run();
	}
	
	private class InitDataRunnable implements Runnable {

		@Override
		public void run() {
			if (!GlobalDefines.FOLDERNAME.equals("") ) {
				viewer.setInput(new File(GlobalDefines.FOLDERNAME).listFiles());
				GuiHelper.checkAllElements(viewer, false);
			}
		}
	}
	
	protected void createProgressShell() {
		progressShell = new Shell(Display.getCurrent());
		progressShell.setText("Progress");
		Rectangle clientArea = progressShell.getClientArea();
		progressShell.setBounds(clientArea.x, clientArea.y, 218, 72);
		Point p = Display.getCurrent().getActiveShell().getLocation();
		p.x += 250;
		p.y += 280;
		progressShell.setLocation(p);
		
		progressBar = new ProgressBar(progressShell, SWT.SMOOTH);
		progressBar.setBounds(clientArea.x, clientArea.y, 200, 32);
	}
	
	public void setDir(String dir) {
		GlobalDefines.FOLDERNAME = dir;
		initData();
	}
	
	@Focus
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Persist
	public void save() {
		dirty.setDirty(false);
	}
	
	// -------------------------------------------------------------------------------------------------------------------------
	// -------------LISTENER----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	private class SelectAllSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			selectAllAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {}
	}
	
	private class DeselectAllSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			deselectAllAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {}
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------ACTION------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	private class SelectAllAction {
		public void run() {
			GuiHelper.checkAllElements(viewer, true);
		}
	}
	
	private class DeselectAllAction {
		public void run() {
			GuiHelper.checkAllElements(viewer, false);
		}
	}

}
