package org.froggy.allinonetool.dialogs;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.froggy.allinonetool.internal.GlobalDefines;

public class StringOverviewDialog extends Dialog {

	private List<String> config;
	private org.eclipse.swt.widgets.List lstConfigItems;
	
	private RemoveStringAction removeStringAction;
	
	private RemoveAllStringsAction removeAllStringsAction;
	
	private Button btnRemove;
	
	private Button btnRemoveAll;

	public StringOverviewDialog(Shell parentShell) {
		super(parentShell);
		try {
			this.config = Files.readAllLines(Paths.get(GlobalDefines.CONFIG), StandardCharsets.UTF_8);
		} catch (IOException e) {
			config = new ArrayList<String>(0);
			e.printStackTrace();
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		lstConfigItems = new org.eclipse.swt.widgets.List(container, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		lstConfigItems.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		btnRemove = new Button(parent, SWT.PUSH);
		btnRemove.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		btnRemove.setText("Remove");
		btnRemove.addSelectionListener(new RemoveStringSelectionListener());
		
		btnRemoveAll = new Button(parent, SWT.PUSH);
		btnRemoveAll.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		btnRemoveAll.setText("Remove All");
		btnRemoveAll.addSelectionListener(new RemoveAllStringsSelectionListener());
		
		initData();
		makeActions();

		return container;
	}
	
	private void initData() {
		lstConfigItems.removeAll();
		for (String string : config) {
			lstConfigItems.add(string);
		}
	}
	
	private void makeActions() {
		removeStringAction = new RemoveStringAction();
		removeAllStringsAction = new RemoveAllStringsAction();
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Current config");
	}

	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}
	
	@Override
	protected void okPressed() {
		super.okPressed();
		
		String newConfig = "";
		for (String string : config) {
			newConfig += string + System.getProperty("line.separator");
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(GlobalDefines.CONFIG));
			writer.write(newConfig);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	//-------------LISTENER----------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------------------
	
	private class RemoveStringSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			removeStringAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("default");
		}
	}
	
	private class RemoveAllStringsSelectionListener implements SelectionListener {

		@Override
		public void widgetSelected(SelectionEvent e) {
			removeAllStringsAction.run();
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			System.out.println("default");
		}
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	//-------------ACTION------------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------------------
	
	private class RemoveStringAction{
		public void run() {
			int[] indices = lstConfigItems.getSelectionIndices();
			
			List<String> toDelete = new ArrayList<String>(indices.length);
			for (int i : indices) {
				toDelete.add(config.get(i));
			}
			config.removeAll(toDelete);
			
			initData();
		}
	}
	
	private class RemoveAllStringsAction{
		public void run() {
			config.clear();
			initData();
		}
	}
}
