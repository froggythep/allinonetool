package org.froggy.allinonetool.helper;

import java.util.regex.Pattern;

public class NameUtil {

	public static String removeFileExtension(String string) {
		String[] splitted = string.split(Pattern.quote("."));
		String toReturn = "";
		
		int i = 0;
		while (i < splitted.length - 1) {
			toReturn += splitted[i];
			if (i < splitted.length -2) {
				toReturn += ".";
			}
			i++;
		}
		toReturn.trim();
		return toReturn;
	}
}
