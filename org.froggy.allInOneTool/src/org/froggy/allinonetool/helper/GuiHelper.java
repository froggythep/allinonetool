package org.froggy.allinonetool.helper;

import java.io.File;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.froggy.allinonetool.adapter.FileModifiedLabelProvider;
import org.froggy.allinonetool.adapter.FileSizeLabelProvider;
import org.froggy.allinonetool.adapter.ViewContentProvider;
import org.froggy.allinonetool.adapter.ViewLabelProvider;
import org.froggy.allinonetool.internal.GlobalDefines;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class GuiHelper {

	public static CheckboxTreeViewer generateFolderTreeViewer(final Composite parent, final int gridSize) {
		CheckboxTreeViewer viewer = new CheckboxTreeViewer(parent, SWT.V_SCROLL | SWT.MULTI);
		viewer.getTree().setLayout(new GridLayout(2, true));
		viewer.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, gridSize, 1));
		viewer.setContentProvider(new ViewContentProvider());
		viewer.getTree().setHeaderVisible(true);
		makeTreeViewerColumns(viewer);

		viewer.addCheckStateListener(new CheckTreeViewerCheckStateListener(viewer));

		return viewer;
	}

	private static void makeTreeViewerColumns(TreeViewer viewer) {
		TreeViewerColumn mainColumn = new TreeViewerColumn(viewer, SWT.NONE);
		mainColumn.getColumn().setText("Name");
		mainColumn.getColumn().setWidth(300);
		mainColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(
				new ViewLabelProvider(createFolderImageDescriptor(), createFileImageDescriptor())));

		TreeViewerColumn modifiedColumn = new TreeViewerColumn(viewer, SWT.NONE);
		modifiedColumn.getColumn().setText("Last Modified");
		modifiedColumn.getColumn().setWidth(100);
		modifiedColumn.getColumn().setAlignment(SWT.RIGHT);
		modifiedColumn.setLabelProvider(
				new DelegatingStyledCellLabelProvider(new FileModifiedLabelProvider(GlobalDefines.DATEFORMAT)));

		TreeViewerColumn fileSizeColumn = new TreeViewerColumn(viewer, SWT.NONE);
		fileSizeColumn.getColumn().setText("Size");
		fileSizeColumn.getColumn().setWidth(100);
		fileSizeColumn.getColumn().setAlignment(SWT.RIGHT);
		fileSizeColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new FileSizeLabelProvider()));
	}

	private static ImageDescriptor createFileImageDescriptor() {
		Bundle bundle = FrameworkUtil.getBundle(ViewLabelProvider.class);
		URL url = FileLocator.find(bundle, new Path("icons/video.png"), null);
		return ImageDescriptor.createFromURL(url);
	}

	private static ImageDescriptor createFolderImageDescriptor() {
		Bundle bundle = FrameworkUtil.getBundle(ViewLabelProvider.class);
		URL url = FileLocator.find(bundle, new Path("icons/folder.png"), null);
		return ImageDescriptor.createFromURL(url);
	}
	
	public static void checkAllElements(CheckboxTreeViewer viewer, boolean checked) {
		File[] files = (File[]) viewer.getInput();
		for (File file : files) {
			viewer.setChecked(file, checked);
			viewer.setSubtreeChecked(file, checked);
		}
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------LISTENER----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------

	private static class CheckTreeViewerCheckStateListener implements ICheckStateListener {

		private CheckboxTreeViewer viewer;

		public CheckTreeViewerCheckStateListener(CheckboxTreeViewer viewer) {
			super();
			this.viewer = viewer;
		}

		@Override
		public void checkStateChanged(CheckStateChangedEvent event) {
			viewer.setSubtreeChecked(event.getElement(), event.getChecked());
		}

	}

}
