/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Lars Vogel <lars.Vogel@gmail.com> - Bug 419770
 *******************************************************************************/
package org.froggy.allinonetool.handlers;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.MApplicationElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.froggy.allinonetool.parts.CheckboxTreeViewerServicePart;
public class OpenHandler {
	
	@Execute
	public void execute(Shell shell, EPartService partService, IEclipseContext context, MApplication application){
		DirectoryDialog dialog = new DirectoryDialog(shell);
		dialog.setMessage("Select Directory");
		final String dir = dialog.open();
		
		if (dir != null) {
			MPart part = partService.findPart(((MApplicationElement)context.get(MApplicationElement.class)).getElementId());
			CheckboxTreeViewerServicePart o = (CheckboxTreeViewerServicePart) part.getObject();
			o.setDir(dir);
		} 	
	}
}
