This is a small tool to clean up file names (remove/replace defined strings) in a defined folder and all subfolders.
In addition it allows you to automatically create folders by recognizing filename patterns and moves files into these folders (it is meant to automatically organize TV shows [show name, season], not implemented yet)

Program can be tested by running executeable\AllInOnetool\eclipse.exe
